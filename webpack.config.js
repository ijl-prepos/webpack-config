const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");
const { merge } = require('webpack-merge');

const getModuleConfig = require('@prepos/utils/get-module-config');

const {
    webpackConfig = {}
} = getModuleConfig();

module.exports = merge({
    output: {
        filename: 'index.js',
        path: path.resolve('dist'),
        libraryTarget: 'umd',
        // TODO: Убрать  dummy
        publicPath: `/static/dummy/1.0.0/`
    },

    resolve: {
        extensions: ['.mjs', '.tsx', '.js', '.jsx', '.ts', '.json']
    },

    module: {
        rules: [{
            test: /\.(jpg|jpeg|png|svg)$/,
            use: [{
                loader: 'file-loader'
            }]
        }, {
            test: /\.(m?js|ts)x?$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                        '@babel/preset-typescript',
                        '@babel/preset-react'
                    ]
                }
            }
        }]
    },

    plugins: [
        new CopyPlugin({
            patterns: [
                { from: "static", to: "static", noErrorOnMissing: true },
                { from: "locales", to: "locales", noErrorOnMissing: true },
            ],
        }),
    ]
}, webpackConfig);
